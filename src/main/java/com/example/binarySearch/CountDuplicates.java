package com.example.binarySearch;

import java.util.Arrays;

public class CountDuplicates {
    public static int countDuplicatesStream1(int[] array, int key) {
        return Arrays.stream(array)
                .filter(value -> value == key)
                .reduce(0, (x, y) -> x + 1);
    }

    public static long countDuplicatesStream2(int[] array, int key) {
        return Arrays.stream(array)
                .filter(value -> value == key)
                .count();
    }

    /**
     * Method counts duplicates for specific value
     * O(log n); Omega (log n)
     *
     * @param array array which is sorted and where user wants to find value
     * @param key   value user wants to find
     * @param left  left index of array
     * @param right fight index of array
     * @return quantity of duplicates for specific key
     */
    public static int countDuplicates(int[] array, int key, int left, int right) {
        int leftIndex = findLeftIndex(array, key, left, right);
        int rightIndex = findRightIndex(array, key, left, right);
        if (leftIndex != rightIndex || leftIndex != -1) {
            return rightIndex - leftIndex + 1;
        }
        return 0;
    }

    /**
     * Method finds first index of appearance for value we want to count duplicates
     * O(log n); Omega (log n)
     *
     * @param array array which is sorted and where user wants to find value
     * @param key   value user wants to find
     * @param left  left index of array
     * @param right fight index of array
     * @return first index of appearance for key user is looking for
     */
    private static int findLeftIndex(int[] array, int key, int left, int right) {
        int mid = (left + right) / 2;
        if (left <= right) {
            if (key == array[mid]) {
                int index = findLeftIndex(array, key, left, mid - 1);
                if (index == -1) {
                    return mid;
                } else {
                    return index;
                }
            }
            if (key > array[mid]) {
                return findLeftIndex(array, key, mid + 1, right);
            }
            return findLeftIndex(array, key, left, mid - 1);
        }
        return -1;
    }

    /**
     * Method finds last index of appearance for value we want to count duplicates
     * O(log n); Omega (log n)
     *
     * @param array array which is sorted and where user wants to find value
     * @param key   value user wants to find
     * @param left  left index of array
     * @param right fight index of array
     * @return last index of appearance for key user is looking for
     */
    private static int findRightIndex(int[] array, int key, int left, int right) {
        int mid = (left + right) / 2;
        if (left <= right) {
            if (key == array[mid]) {
                int index = findRightIndex(array, key, mid + 1, right);
                if (index == -1) {
                    return mid;
                } else {
                    return index;
                }
            }
            if (key > array[mid]) {
                return findRightIndex(array, key, mid + 1, right);
            }
            return findRightIndex(array, key, left, mid - 1);
        }
        return -1;
    }

}

