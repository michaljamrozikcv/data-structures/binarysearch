package com.example.binarySearch;

public class BinarySearch {
    /**
     * Binary search - iterative version - O(log n); Omega(1)
     *
     * @param array array which is sorted and where user wants to find value
     * @param key   value user wants to find
     * @return index of array, if key is found
     */
    public static int binarySearchIteration(int[] array, int key) {
        int left = 0;
        int right = array.length - 1;
        while (left <= right) {
            int mid = (left + right) / 2;
            if (key == array[mid]) {
                return mid;
            } else if (key < array[mid]) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return -1;
    }

    /**
     * Binary search - recursive version O(log n); Omega(log n)
     *
     * @param array array which is sorted and where user wants to find value
     * @param key   value user wants to find
     * @param left  left index of array
     * @param right fight index of array
     * @return index of array, if key is found
     */
    public static int binarySearchRecursion(int[] array, int key, int left, int right) {
        int mid = (left + right) / 2;
        if (left <= right) {
            if (key == array[mid]) {
                return mid;
            }
            if (key > array[mid]) {
                return binarySearchRecursion(array, key, mid + 1, right);
            }
            return binarySearchRecursion(array, key, left, mid - 1);
        }
        return -1;
    }
}
