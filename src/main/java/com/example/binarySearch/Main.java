package com.example.binarySearch;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {-2, 0, 0, 0, 1, 2, 3, 3, 4, 5, 5, 5};
        int number = 1;
        System.out.println("====================================");
        System.out.println(" On what index array contains number: " + number);
        System.out.println("====================================");
        System.out.println("Array: " + Arrays.toString(array));
        System.out.println("Binary search built in Java - index found: " + Arrays.binarySearch(array, number));
        System.out.println("My binary search by iteration - index found: " +
                BinarySearch.binarySearchIteration(array, number));
        System.out.println("My binary search by recursion - index found: " +
                BinarySearch.binarySearchRecursion(array, number, 0, array.length - 1));
        int number2 = 3;
        System.out.println("====================================");
        System.out.println(" How many duplicates of " + number2);
        System.out.println("====================================");
        System.out.println("Array: " + Arrays.toString(array));
        System.out.println("Stream function1 - no. of duplicates: " + CountDuplicates.countDuplicatesStream1(array, number2));
        System.out.println("Stream function2 - no. of duplicates: " + CountDuplicates.countDuplicatesStream2(array, number2));
        System.out.println("My function based on binary search- no. of duplicates: "
                + CountDuplicates.countDuplicates(array, number2, 0, array.length - 1));
    }
}
